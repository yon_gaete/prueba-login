from django.db import models
from django.contrib.auth.models import User
from administracion.models import Tienda
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class VendedorUsuario(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE, null=True)

@receiver(post_save, sender=User)
def create_user_vendedorusuario(sender, instance, created, **kwargs):
    if created:
        VendedorUsuario.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_vendedorusuario(sender, instance, **kwargs):
    instance.vendedorusuario.save()