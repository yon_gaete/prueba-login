from django import forms
from django.contrib.auth.models import User
from .models import VendedorUsuario,Tienda

class VendedorUsuarioForm(forms.ModelForm):
    class Meta():
        model = VendedorUsuario
        fields = ('tienda',)
        labels = {
            'tienda' : 'Tienda'
        }
    def __init__(self, *args, **kwargs):
        super(VendedorUsuarioForm, self).__init__(*args,**kwargs)
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            

class RegistrarForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name', 'last_name',)
        labels = {
            'username' : 'Nombre de usuario',
            'email' : 'Correo',
            'password': 'Contraseña',
            'first_name': 'Nombres',
            'last_name': 'Apellidos'

        }
        help_texts = {
           'username': '',
        }
        error_messages = {
            'username': {
                'max_length': 'Máximo 150 carácteres',
                'required': 'Requerido'
            },
            'password': {
                'required': 'Requerido'
            },
            'email': {
                'required': 'Requerido'
            },
            'first_name': {
                'required': 'Requerido'
            },
            'last_name': {
                'required': 'Requerido'
            },
        }
    
    def __init__(self, *args, **kwargs):
        super(RegistrarForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
