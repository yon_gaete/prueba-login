from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .forms import RegistrarForm, VendedorUsuarioForm
# Para iniciar sesión

def usuario_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('administracion:index'))


def usuario_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('administracion:index'))
    else:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('administracion:index'))
                else:
                    return HttpResponse("Tu cuenta está inactiva.")
            else:
                print("username: {} - password: {}".format(username, password))
                return HttpResponse("Datos inválidos")
        else:
            return render(request, 'gestion_usuarios/login.html', {})

def registrar(request):
    registrado = False
    if request.method == 'POST':
        user_form = RegistrarForm(data=request.POST)
        vendedorusuario_form = VendedorUsuarioForm(data=request.POST)
        if user_form.is_valid() and vendedorusuario_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()         
            vendedorusuario_form.save(commit=False)
            registrado = True
            return HttpResponseRedirect(reverse('gestion_usuarios:login'))
        else:
            print(user_form.errors, vendedorusuario_form.errors)
    else:
        user_form = RegistrarForm()
        vendedorusuario_form = VendedorUsuarioForm()

    return render(request, 'gestion_usuarios/registrar.html',
                  {'user_form': user_form,
                   'vendedorusuario_form': vendedorusuario_form,
                   'registrado': registrado})
