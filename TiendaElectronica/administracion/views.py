from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Tienda
from django.utils import timezone
from django.urls import reverse
from django.views.generic import DetailView, ListView
# Create your views here.


class PostListView(ListView):
    model = Tienda
    template_name = "administracion/index.html"


