from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Tienda(models.Model):
    nombre_tienda = models.CharField(max_length=200, unique=True)
    direccion = models.CharField(max_length=200)
    ciudad = models.CharField(max_length=200)
    comuna = models.CharField(max_length=200)
    telefono = models.IntegerField()
    email = models.EmailField()
    encargado = models.ForeignKey(User, on_delete=models.CASCADE, related_name='encargado',null=True)

    def __str__(self):
        return self.nombre_tienda


