from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from . import views

app_name = 'administracion'

urlpatterns = [
    path('', views.PostListView.as_view(), name='index')
]
